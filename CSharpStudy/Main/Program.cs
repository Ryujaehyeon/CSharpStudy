﻿//#define _7bitEncodedInt_BinaryWrite

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#if Study
using AbstractClass;
using PropertiesInterface;
using PropertiesAbstractInterface;
using MoreOnArray;
using ConstraintsOnTypeParameters;
using EnumerableGeneric;
using CSharpStudy;
using CSharpStudy.Study12_Exception;
#endif
using CSharpStudy.Class;
using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;
using IronPython.Hosting;
using System.Threading;

namespace CSharpStudy
{
    class Program
    {
#if Global_Static
        public static T CreateInstance<T>() where T : new() => new T();
#endif
#if UsingException
        static uint MergeARGB(uint alpha, uint red, uint green, uint blue)
        {
            uint[] args = new uint[] { alpha, red, green, blue };
            foreach(uint arg in args)
            {
                if (arg > 255)
                    throw new InvalidArgumentException()
                    {
                        Argument = arg,
                        Range = "0~255"
                    };
            }

            return (alpha   << 24 & 0xFF000000) |
                   (red     << 16 & 0x00FF0000) |
                   (green   <<  8 & 0x0000FF00) |
                   (blue          & 0x000000FF);
        }
#endif
        static void Main(string[] args)
        {
            #region StudyCode
            ///////////////////////////////////////////
#if true
            Counter counter = new Counter();

            Thread incThread = new Thread(new ThreadStart(counter.Increase));
            Thread DecThread = new Thread(new ThreadStart(counter.Decrease));

            incThread.Start();
            Console.WriteLine(counter.Count);

            DecThread.Start();
            Console.WriteLine(counter.Count);

            incThread.Join();
            DecThread.Join();

            Console.WriteLine(counter.Count);

#endif
#if _7bitEncodedInt_BinaryWrite
            //Binaryfile
            BinaryWriter bw = new BinaryWriter(new FileStream("a.dat", FileMode.Create));

            //bw.Write(int.MaxValue);

            //문자열이 255가 넘어가면 들어가는 길이값의 방식이 7bitEncodedInt구조체로 인코딩됨
            
            const int max = 127 * 1;
            string[] array127x = new string[max];
            for (int i = 0; i < max; i++)
            {
                array127x[i] = "O";
            }
            int corrunt = array127x.Length;

            bw.Write("o_o"); 

            //bw.Write(uint.MaxValue);
            //bw.Write("안녕하세요!");
            //bw.Write(double.MaxValue);
            long ilength = bw.BaseStream.Length;
            bw.Close();

            BinaryReader br = new BinaryReader(new FileStream("a.dat", FileMode.Open));

            Console.WriteLine("File Size : {0} Bytes", br.BaseStream.Length);
            //Console.WriteLine("{0}", br.ReadInt32());
            Console.WriteLine("{0}", br.ReadString());
            //Console.WriteLine("{0}", br.ReadUInt32());
            //Console.WriteLine("{0}", br.ReadString());
            //Console.WriteLine("{0}", br.ReadDouble());
            br.Close();
#endif
#if Seek예제
            //파일 쓰기
            Stream outStream = new FileStream("a.dat", FileMode.Create);
            Console.WriteLine("Position : {0}", outStream.Position);

            outStream.WriteByte(0x01);
            Console.WriteLine("Position : {0}", outStream.Position);

            outStream.WriteByte(0x02);
            Console.WriteLine("Position : {0}", outStream.Position);

            outStream.WriteByte(0x03);
            Console.WriteLine("Position : {0}", outStream.Position);

            outStream.Seek(5, SeekOrigin.Current);
            Console.WriteLine("Position : {0}", outStream.Position);

            outStream.WriteByte(0x04);
            Console.WriteLine("Position : {0}", outStream.Position);
            outStream.Close();

            //파일 읽기
            Console.WriteLine();
            Stream inStream = new FileStream("a.dat", FileMode.Open);

            byte[] rbytes = new byte[9];
            int i = 0;
            while (inStream.Position < inStream.Length)
                rbytes[i++] = (byte)inStream.ReadByte();

            inStream.Seek(0, SeekOrigin.Current);
            inStream.Position = 0;
            Console.WriteLine("Position : {0}", inStream.Position);

            inStream.ReadByte();
            Console.WriteLine("Position : {0}", inStream.Position);

            inStream.ReadByte();
            Console.WriteLine("Position : {0}", inStream.Position);

            inStream.ReadByte();
            Console.WriteLine("Position : {0}", inStream.Position);

            inStream.Seek(5, SeekOrigin.Current);
            Console.WriteLine("Position : {0}", inStream.Position);

            inStream.ReadByte();
            Console.WriteLine("Position : {0}", inStream.Position);
            inStream.Close();
#endif
#if FILE_IO_2
            long someValue = 0x123456789ABCDEF;
            Console.WriteLine("{0, -1} : 0x{1:X16}", "Original Data", someValue);

            Stream outStream = new FileStream("a.dat", FileMode.Create);
            byte[] wBytes = BitConverter.GetBytes(someValue);

            Console.Write("{0,-13} : ", "Byte array");

            foreach (byte b in wBytes)
                Console.Write("{0:X2} ", b);
            Console.WriteLine();

            outStream.Write(wBytes, 0, wBytes.Length);
            outStream.Close();

            Stream inStream = new FileStream("a.dat", FileMode.Open);
            byte[] rbytes = new byte[8];

            int i = 0;
            while (inStream.Position < inStream.Length)
                rbytes[i++] = (byte)inStream.ReadByte();

            long readValue = BitConverter.ToInt64(rbytes, 0);

            Console.WriteLine("{0, -13} : 0x{1:X16} ", "Read Data", readValue);
            inStream.Close();
#endif
#if FILE_IO_1
            //args = new string[] { "MyDirectory" , "Directory" };
            args = new string[2];
            Console.WriteLine("usage : File_IO.exe <Path> [Type/Directory]");
            Console.WriteLine("args[0] : [Type/Directory]");
            args[0] = Console.ReadLine() as string;
            Console.WriteLine("args[1] : [Type/Directory]");
            args[1] = Console.ReadLine() as string;

            //파일 입출력
            if (args.Length == 0)
            {
                Console.WriteLine("usage : File_IO.exe <Path> [Type/Directory]");
                return;
            }

            string path = args[0];
            string type = args[1];
            if (args.Length > 1)
            {
                type = type.First().ToString().ToUpper() + type.Remove(0, 1).ToLower();
            }
                
            if (File.Exists(path) || Directory.Exists(path))
            {
                if (type == "File")
                    File.SetLastWriteTime(path, DateTime.Now);
                else if (type == "Directory")
                    Directory.SetLastWriteTime(path, DateTime.Now);
                else
                {
                    File_IO.OnWrongPathType(path);
                    return;
                }
                Console.WriteLine("Update {0} {1}", path, type);
            }
            else
            {
                if (type == "File")
                    File.Create(path).Close();
                else if (type == "Directory")
                    Directory.CreateDirectory(path);
                else
                {
                    File_IO.OnWrongPathType(path);
                    return;
                }
                Console.WriteLine("Created {0} {1}", path, type);
            }
#endif
#if ScriptPython
            ScriptEngine engine = Python.CreateEngine();

            //파이썬 
            //변수 선언 및 초기화
            ScriptScope scope = engine.CreateScope();
            scope.SetVariable("n", "Ryu");
            scope.SetVariable("p", "010-4761-0000");

            //소스작성
            ScriptSource source = engine.CreateScriptSourceFromString(
                @"
class NameCard : 
    name = ''
    phone = ''
    
    def __init__(self,name,phone):
        self.name = name
        self.phone = phone

    def printNameCard(self):
        print self.name + ', ' + self.phone

NameCard(n,p)
");
            dynamic result = source.Execute(scope);
            result.printNameCard();
            Console.WriteLine("{0}, {1}", result.name, result.phone);

#endif
#if COMInterop
            string savePath = System.IO.Directory.GetCurrentDirectory();
            string[,] array = new string[,]
            {
                {"Date  :", "2018" },
                {"Log   :", "ExcelLog" },
                {"Log2  :", "ExcelLog2" }
            };

            Console.WriteLine("Creating Excel document in old way...");
            COMInterop old_Log = new COMInterop();
            COMInterop.OldWay(array, savePath);


            Console.WriteLine("Creating Excel document in new way...");
            //COMInterop new_Log = new COMInterop();
            COMInterop.NewWay(array, savePath);
#endif
#if Simplelambda
            Calculate_my calc = (a, b) => a + b;
            Console.WriteLine("{0} + {1} = {2}", 3, 4, calc(3, 4));

            Func<int> func1 = () => 10;
            Console.WriteLine(func1);

            Func<int, int> func2 = (x) => x * 2;

#endif
#if StudyDelegateEvent
            //이벤트 인스턴스 생성
            EventDelegate eventDelegate = new EventDelegate();
            //3-1. 이벤트 인스턴스에 사용할 이벤트 핸들러(메서드) 등록
            eventDelegate.SomethingHappened += new Class.EventHandler(EventDelegate.MyHandler);

            for (int i = 1; i < 30; i++)
            {
                eventDelegate.DoSomething(i);
            }
            //클래스의 메서드내에서만 사용되는 방식이기 때문에
            //실제로 클래스 밖에서 아래처럼 값을 받는 등의 방식으로 사용하면 에러발생
            //eventDelegate.SomethingHappened("테스트");

#endif
#if StudyDelegate
            UsingCallback MyCompere = new UsingCallback();

            /////////////
            /////////////

            int[] array = { 3, 7, 4, 2, 10 };
            Console.WriteLine("Sortng ascending...");
            MyCompere.BubbleSort(array, new Compare(MyCompere.AscendCompare));

            for (int i = 0; i < array.Length; i++)
                Console.WriteLine("{0} ", array[i]);

            int[] array2 = { 7, 2, 8, 10, 11 };
            Console.WriteLine("\nSortng ascending...");
            MyCompere.BubbleSort(array2, new Compare(MyCompere.DescendCompare));

            for (int i = 0; i < array2.Length; i++)
                Console.WriteLine("{0} ", array2[i]);

            //아래 구문에서
            //new Compare<int>(DescendCompare)가 <자료형>을 명시해주지 않아서인지 
            //일반화된 메서드를 사용하지 않는데
            //DescendCompare<자료형>로 자료형을 명시하지 않아서
            //Compare<int>()로 받은 메서드가 DescendCompare이니 
            //그대로 기본 메서드를 사용한듯 하다.
            //int형 메서드를 주석처리하면 일반화된 메서드 사용함
            //자료형을 명시하지 않아서 그냥 사용한건지, 최적화를 해서 사용하지 않는게 좋다고
            //판단해서 이렇게 사용하는 건지는 모르겠다.
            int[] array3 = { 8, 7, 1, 4, 6 };
            Console.WriteLine("\nSortng ascending...");
            MyCompere.BubbleSort<int>(array3, new Compare<int>(MyCompere.DescendCompare));

            for (int i = 0; i < array.Length; i++)
                Console.WriteLine("{0} ", array3[i]);

            //이쪽에선 string으로 구현된 AscendCompare메서드가 없어서
            //일반화된 메서드를 사용
            string[] array4 = { "abc", "def","ghi","jkl","mno" };
            Console.WriteLine("\nSortng descending...");
            MyCompere.BubbleSort<string>(array4, new Compare<string>(MyCompere.AscendCompare));

            for (int i = 0; i < array2.Length; i++)
                Console.WriteLine("{0} ", array4[i]);

            string[] array5 = { "abc", "def", "ghi", "jkl", "mno" };
            Console.WriteLine("\nSortng descending...");
            MyCompere.BubbleSort<string>(array5, new Compare<string>(MyCompere.AscendCompare));

            for (int i = 0; i < array2.Length; i++)
                Console.WriteLine("{0} ", array4[i]);

            //무명 메소드 델리게이트
            int[] array6 = { 11, 16, 3, 8, 43 };
            Console.WriteLine("\nNameless Delegate Sortng Ascending...");
            MyCompere.BubbleSort<int>(array, delegate (int a, int b)
             {
                 if (a > b)
                     return 1;
                 else if (a == b)
                     return 0;
                 else
                     return -1;
             });

            for (int i = 0; i < array.Length; i++)
                Console.Write("{0} ", array[i]);

            int[] array7 = { 7, 2, 8, 10, 11 };
            Console.WriteLine("\nNameless Delegate Sortng Descending...");
            MyCompere.BubbleSort<int>(array, delegate (int a, int b)
            {
                if (a < b)
                    return 1;
                else if (a == b)
                    return 0;
                else
                    return -1;
            });

            for (int i = 0; i < array2.Length; i++)
                Console.Write("{0} ", array2[i]);

            Console.WriteLine();

#endif
#if MyTrace
            StackTrace MyStackTrace = new StackTrace{ a = 1};

            try
            {
                int i = 1;
                Console.WriteLine( 3/--i );
                Console.WriteLine( 3/--MyStackTrace.a);

            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine(e.StackTrace);
            }
#endif
#if UsingException
            try
            {
                Console.WriteLine("0x{0:X}", MergeARGB(255, 111, 111, 111));
                Console.WriteLine("0x{0:X}", MergeARGB(1, 65, 192, 128));
                Console.WriteLine("0x{0:X}", MergeARGB(0, 255, 255, 300));
            }
            catch(InvalidArgumentException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Argument:{0}, Range:{1}", e.Argument, e.Range);
            }

#endif
#if Try_Catch_AND_Finally
            Finally MyFinally = new Finally();

            try
            {
                Console.WriteLine("제수를 입력하세요. : ");
                string temp = Console.ReadLine();
                int divisor = Convert.ToInt32(temp);

                Console.WriteLine("피제수를 입력하세요. : ");
                temp = Console.ReadLine();
                int dividend = Convert.ToInt32(temp);

                Console.WriteLine("{0} / {1} : {2}" ,
                    divisor,dividend, MyFinally.Divide(divisor, dividend));

            }
            catch(FormatException e)
            {
                Console.WriteLine("에러 : " + e.Message);
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine("호출자에서 Divide() 예외 받음");
                Console.WriteLine("에러 : " + e.Message);
            }
            finally
            {
                Console.WriteLine("프로그램 종료");
            }

#endif
#if MyThrow
            Throw MyThrow = new Throw();
            try
            {
                MyThrow.DoSomething(1);
                MyThrow.DoSomething(9);
                MyThrow.DoSomething(13);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
#endif
#if TryCatch
            TryCatch MyTry = new TryCatch() { Arr = new int[] { 1, 2, 3 } };
            try
            {
                for (int i = 0; i < 5; i++)
                    Console.WriteLine(MyTry.Arr[i]);
            }
            catch(IndexOutOfRangeException e)
            {
                Console.WriteLine("예외 발생 : {0}", e.Message);
            }
            MyTry.ProgramEnd();
#endif
#if EnumerableGeneric
            MyList<string> str_list = new MyList<string>();
            str_list[0] = "abc";
            str_list[1] = "def";
            str_list[2] = "ghi";
            str_list[3] = "jkl";
            str_list[4] = "mno";

            foreach (string str in str_list)
                Console.WriteLine(str);
            Console.WriteLine();

            MyList<int> int_list = new MyList<int>();
            int_list[0]= 0;
            int_list[1]= 1;
            int_list[2]= 2;
            int_list[3]= 3;
            int_list[4]= 4;

            foreach (int no in int_list)
                Console.WriteLine(no);
            Console.WriteLine();
#endif
#if GenericCollection
            //List
            Console.WriteLine("List");
            List<int> list = new List<int>();

            for(int i = 0; i < 5; i++)
                list.Add(i);

            foreach (int element in list)
                Console.Write("{0}", element);
            Console.WriteLine();

            list.RemoveAt(2);

            foreach (int element in list)
                Console.Write("{0}", element);
            Console.WriteLine();

            list.Insert(2, 7);

            foreach (int element in list)
                Console.Write("{0}", element);
            Console.WriteLine();

            //queue
            Console.WriteLine("\nqueue");
            Queue<int> queue = new Queue<int>();

            for (int i = 0; i < 5; i++)
                queue.Enqueue(i);

            while (queue.Count > 0)
                Console.WriteLine(queue.Dequeue());

            Stack<int> stack = new Stack<int>();
            for (int i = 5; i < 5; i++)
                stack.Push(i);

            while (stack.Count > 0)
                Console.WriteLine(stack.Pop());

            ///dictionary
            Console.WriteLine("\ndictionary");
            Dictionary<string, string> dic  = new Dictionary<string, string>();
            dic["하나"] = "one";
            dic["둘"]   = "two";
            dic["셋"]   = "three";
            dic["넷"]   = "four";
            dic["다섯"] = "five";

            Console.WriteLine(dic["하나"]);
            Console.WriteLine(dic["둘"]  );
            Console.WriteLine(dic["셋"]  );
            Console.WriteLine(dic["넷"]  );
            Console.WriteLine(dic["다섯"]);
#endif
#if Generic
            Console.WriteLine("1.");
            StructArray<int> a = new StructArray<int>(3);
            a.Array[0] = 0;
            a.Array[1] = 1;
            a.Array[2] = 2;
            foreach (int i in a.Array)
                Console.WriteLine("{0}", i);
            Console.WriteLine();

            Console.WriteLine("2.");
            RefArray<StructArray<double>> b = new RefArray<StructArray<double>>(3);
            b.Array[0] = new StructArray<double>(5);
            b.Array[1] = new StructArray<double>(10);
            b.Array[2] = new StructArray<double>(15);
            foreach (StructArray<double> i in b.Array)
                Console.WriteLine("{0}", i.Array.Length);
            Console.WriteLine();

            Console.WriteLine("3.");
            BaseArray<Base> c = new BaseArray<Base>(3);
            c.Array[0] = new Base();
            c.Array[1] = new Derived();
            c.Array[2] = CreateInstance<Base>();
            foreach (Base i in c.Array)
                Console.WriteLine("{0}", i);
            Console.WriteLine();

            Console.WriteLine("4.");
            BaseArray<Derived> d = new BaseArray<Derived>(3);
            d.Array[0] = new Derived();
            d.Array[1] = CreateInstance<Derived>();
            d.Array[2] = CreateInstance<Derived>();
            foreach (Derived i in d.Array)
                Console.WriteLine("{0}", i);
            Console.WriteLine();

            Console.WriteLine("5.");
            BaseArray<Derived> e = new BaseArray<Derived>(3);
            e.CopyArray<Derived>(d.Array);
            foreach (Derived i in e.Array)
                Console.WriteLine("{0}", i);
            Console.WriteLine();

            Console.WriteLine("6.");
            BaseArray_<BaseInterfaceClass> f = new BaseArray_<BaseInterfaceClass>(3);
            f.Array[0] = new BaseInterfaceClass();
            f.Array[1] = new Derived_();
            f.Array[2] = CreateInstance<BaseInterfaceClass>();
            foreach (BaseInterfaceClass i in f.Array)
                Console.WriteLine("{0}", i);
            Console.WriteLine();

            Console.WriteLine("7.");
            BaseArray_<Derived_> g = new BaseArray_<Derived_>(3);
            g.Array[0] = new Derived_();
            g.Array[1] = CreateInstance<Derived_>();
            g.Array[2] = CreateInstance<Derived_>();
            foreach (Derived_ i in g.Array)
                Console.WriteLine("{0}", i);
            Console.WriteLine();

            Console.WriteLine("8.");
            BaseArray_<Derived_> h = new BaseArray_<Derived_>(3);
            h.CopyArray<Derived_>(g.Array);
            foreach (Derived_ i in h.Array)
                Console.WriteLine("{0}", i);
            Console.WriteLine();

            BaseArray_<Derived_> z = new BaseArray_<Derived_>(1);
#endif
#if Study

            int string_hash_key = 0;
            Hashtable ht = new Hashtable
            {
                [string_hash_key] = "zero"
            };

            MyArray myArray = new MyArray
            {
                MyCustomArray = new int[] { 90, 70, 88, 79, 97 },
                _2DArray = new int[,] { { 91, 73, 59, 95, 77 }, { 99, 84, 22, 19, 92 } },
                _3DArray = new int[,,]
                {
                    { { 91 }, { 73 } }, { { 91 }, { 73 } }
                },
                _JaggedArray = new int[][] 
                {
                    new int[] { 91, 73, 59, 95, 77 },
                    new int[] { 90, 70, 88, 79, 97 }
                }
            };


            int sum = 0;

            foreach (int score in myArray.MyCustomArray)
                sum += score;

            Console.WriteLine("{0} ", sum);

            Array.Sort(myArray.MyCustomArray);

            foreach (int score in myArray.MyCustomArray)
                Console.WriteLine("{0} ", score);

            Array.ForEach<int>(myArray.MyCustomArray, new Action<int>(MyArray.Print));
            Console.WriteLine();

            Console.WriteLine("Number of dimensions : {0}", myArray.MyCustomArray.Rank);

            Console.WriteLine("Binary Search : 90 is at {0}",
                Array.BinarySearch<int>(myArray.MyCustomArray, 90));

            Console.WriteLine("Binary Search : 79 is at {0}",
                Array.IndexOf<int>(myArray.MyCustomArray, 79));

            Console.WriteLine("Everyone passed ? : {0}",
                Array.TrueForAll<int>(myArray.MyCustomArray, MyArray.CheckPassed));

            int index = Array.FindIndex<int>(myArray.MyCustomArray,
                delegate (int score)
                {
                    if (score > 60)
                        return true;
                    else
                        return false;
                });

            myArray.MyCustomArray[index] = 61;

            Console.WriteLine("Everyone passes ? : {0}",
                Array.TrueForAll<int>(myArray.MyCustomArray, MyArray.CheckPassed));

            //직접구현하지 않아도 deep copy가 지원됨
            int[] newscore = myArray.MyCustomArray;
            Console.WriteLine("Old length of score : {0}", newscore.GetLength(0));

            Array.Resize<int>(ref newscore, 20);

            Console.WriteLine("Old length of score : {0}", newscore.Length);

            Array.ForEach<int>(newscore, new Action<int>(MyArray.Print));
            Console.WriteLine();

            Array.Clear(newscore, 3, 7);

            Array.ForEach<int>(newscore, new Action<int>(MyArray.Print));
            Console.WriteLine();

            
            Product product_1 = new MyProduct()
            { ProductDate = new DateTime(2018, 4, 2) };

            Console.WriteLine("Product : {0}, Product Date : {1}", product_1.SerialID, product_1.ProductDate);

            Product product_2 = new MyProduct()
            { ProductDate = new DateTime(2018, 4, 3) };

            Console.WriteLine("Product : {0}, Product Date : {1}", product_2.SerialID, product_2.ProductDate);

            NameValue name = new NameValue()
            { Name = "ryu", Value = "jaehyeon" };

            NameValue height = new NameValue()
            { Name = "height", Value = "183cm" };

            NameValue weight = new NameValue()
            { Name = "weight", Value = "78.9kg" };

            Console.WriteLine("{0,-7} : {1}", name.Name, name.Value);
            Console.WriteLine("{0,-7} : {1}", height.Name, height.Value);
            Console.WriteLine("{0,-7} : {1}", weight.Name, weight.Value);


            //Abstract Class
            AbstractBase obj = new Derived();
            obj.AbstractMethodA();
            obj.PublicMethodA();

            //interface로 다중 상속 흉내내기
            FlyingCar flyingCar = new FlyingCar();
            //Run, Fly로 나뉘어 있지만, 
            //아래 두개의 메소드가 내부에서 호출하는 인스턴스는 Car, Plane이며 이 둘이 호출하는 메소드는 Ride()로 동일
            flyingCar.Run();
            flyingCar.Fly();

            //flyingcar에 인스턴스 다중 상속시 사용가능
            //IRunnable runnable = flyingCar as IRunnable;
            //runnable.Run();

            //IFlyable flyable = flyingCar as IFlyable;
            //flyable.Fly();

            Console.WriteLine("---------------------------------------------");
            if (flyingCar is IRunnable)
            {
                Console.WriteLine("Yes");
            }
            else
            {
                Console.WriteLine("No");
            }

            if (flyingCar is IFlyable)
            {
                Console.WriteLine("Yes");
            }
            else
            {
                Console.WriteLine("No");
            }

            Console.WriteLine("---------------------------------------------");

            uint a = uint.MaxValue;
            Console.WriteLine(a + 2);

            int[] iArr = new int[] { 1, 2, 3, 4 };
            int j = 2;
            foreach (int i in iArr)
            {
                Console.WriteLine("{0} : {1}", i, j);
                if (i == j)
                {
                    Console.WriteLine("Yes\n");
                }
                else
                {
                    Console.WriteLine("No\n");
                }
            }

            StudyClass studyClass = new StudyClass { Ia = 0, Sname = "test" };
            Console.WriteLine("{0} {1}", studyClass.Ia, studyClass.Sname);
            studyClass.Ia = 1;
            studyClass.Sname = "standard";
            Console.WriteLine("{0} {1}", studyClass.Ia, studyClass.Sname);

            //studyClass.ib = 0;
            studyClass.Setib(20);
            Console.WriteLine("{0}", studyClass.Getib());
            studyClass.WriteLog("Yeeeeeeee");

            studyClass.Ipri_int = 20;
#else
#endif
            /////////////////////////////////////////
            #endregion
        }
    }
}
