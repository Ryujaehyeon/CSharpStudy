﻿namespace CSharpStudy
{
    interface ILogger
    {
        void WriteLog(string message);
    }
}
