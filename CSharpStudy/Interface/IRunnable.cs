﻿namespace CSharpStudy
{
    interface IRunnable
    {
        void Run();
    }
}
