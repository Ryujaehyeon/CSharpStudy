﻿namespace CSharpStudy
{
    interface IFlyable
    {
        void Fly();
    }
}
