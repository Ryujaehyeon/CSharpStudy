﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpStudy.Class
{
    class Throw
    {
        public void DoSomething(int arg)
        {
            if(arg <10)
            {
                Console.WriteLine("arg : {0}", arg);
            }
            else
            {
                throw new Exception("arg가 10보다 큽니다.");
            }
        }
    }
}
