﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpStudy.Class
{
    //1. 이벤트 델리게이트 선언
    delegate void EventHandler(string message);
    class EventDelegate
    {
        //3.사용할 이벤트 핸들러의 매개변수는 이벤트 델리게이트와 동일하게 맞추기
        static public void MyHandler(string message)
        {
            //구현부
            Console.WriteLine(message);
        }
        //1-1. 이벤트 델리게이트 인스턴트 생성
        public event EventHandler SomethingHappened;
        public void DoSomething(int number)
        {
            int temp = number % 10;
            if (temp != 0 && temp % 3 == 0)
                //4. 사용(호출), 선언한 클래스 내에서만 사용 가능
                SomethingHappened(String.Format("{0} : 짝", number));
        }
    }
}
