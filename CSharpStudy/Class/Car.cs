﻿using System;

namespace CSharpStudy
{
    class Car : IRunnable, IRide
    {
        public void Run()
        {
            Console.WriteLine("Run!");
        }

        public void Ride()
        {
            Run();
        }
    }
}
