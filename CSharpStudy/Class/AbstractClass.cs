﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClass
{
    //추상클래스
    abstract class AbstractBase
    {
        protected void PrivateMethodA()
        {
            Console.WriteLine("AbstractBase.PrivateMethodA()");
        }

        public void PublicMethodA()
        {
            Console.WriteLine("AbstractBase.PublicMethodA()");
        }

        //추상메서드
        public abstract void AbstractMethodA();
    }

    class Derived : AbstractBase
    { 
        //추상 클래스를 상속 후 파생클래스를 생성하고 추상클래스의 추상메서드를 구현하게끔 강제하는 기능
        //AbstractMethodA()를 구현하지 않으면 에러 발생
        public override void AbstractMethodA()
        {
            Console.WriteLine("Derived.AbstractMethodA()");
            PrivateMethodA();
        }
    }
}
