﻿using System;

namespace CSharpStudy
{
    class StudyClass : ILogger
    {
        public int Ipri_int { get; set; }
        public int Ia { get; set; }
        public string Sname { get; set; }
        int Ib { get; set; }

        public int Getib() { return Ib; }
        public void Setib(int _ib) { Ib = _ib; }

        public void WriteLog(string message)
        {
            Console.WriteLine("{0}", message);
            //throw new NotImplementedException();
        }
    }
}
