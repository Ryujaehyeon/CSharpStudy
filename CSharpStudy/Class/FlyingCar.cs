﻿using System;

namespace CSharpStudy
{
    //다중 상속이 아닌 포함 기법으로 클래스를 필드에 선언해 사용하는 방법
    //이 클래스에서 인터페이스 다중 상속하거나, 
    //필드에 선언된 클래스에 인터페이스 다중 상속하거나, 
    //둘 중 하나의 방법
    class FlyingCar
    {
        Car car = new Car();
        Plane plane = new Plane();

        public void Run()
        {
            car.Ride();
        }

        public void Fly()
        {
            plane.Ride();
        }
    }
}
