﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CSharpStudy;
using CSharpStudy.Interface;

namespace ConstraintsOnTypeParameters
{
    class StructArray<T> where T:struct
    {
        public T[] Array { get; set; }
        public StructArray(int size)
        {
            Array = new T[size];
        }
    }

    class RefArray<T> where T:class
    {
        public T[] Array { get; set; }
        public RefArray(int size)
        {
            Array = new T[size];
        }
    }


    class Base { }
    class Derived : Base { }
    class BaseArray<U> where U : Base
    {
        public U[] Array { get; set; }
        public BaseArray(int size)
        {
            Array = new U[size];
        }

        public void CopyArray<T>(T[] Source) where T : U
        {
            Source.CopyTo(Array, 0);
        }
    }

    //Q. 인터페이스를 구현하는 클래스로 
    //   형식 매개 변수를 제약하는 일반화 코드
    //
    //A. 어느 클래스를 형식 매개 변수로 인터페이스를 구현하는 다른 클래스만
    //   받는(제약하는) 일반화 코드를 만들라는 건지? 뭔 말인지 모르겠다.
    //일단 대충 따라서 만들어봄
    class BaseInterfaceClass : IBaseInterface { }
    class Derived_ : BaseInterfaceClass { }
    class BaseArray_<U> where U : IBaseInterface
    {
        public U[] Array { get; set; }
        public BaseArray_(int size)
        {
            Array = new U[size];
        }

        public void CopyArray<T>(T[] Source) where T : U
        {
            Source.CopyTo(Array, 0);
        }
    }


}
