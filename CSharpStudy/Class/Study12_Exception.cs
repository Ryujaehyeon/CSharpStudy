﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpStudy.Study12_Exception
{
    class TryCatch
    {
        public int[] Arr { get; set; }

        public void ProgramEnd()
        {
            Console.WriteLine("END!");
        }
    }
}
