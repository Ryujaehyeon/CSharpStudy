﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoreOnArray
{
    class MyArray
    {
        public static bool CheckPassed(int score)
        {
            if (score >= 60)
                return true;
            else
                return false;
        }

        public static void Print(int value)
        {
            Console.Write("{0,-3} ", value);
        }

        public int[] MyCustomArray { set; get; }
        public int[,] _2DArray { set; get; }
        public int[,,] _3DArray { set; get; }
        public int[][] _JaggedArray { set; get; }

    }
}
