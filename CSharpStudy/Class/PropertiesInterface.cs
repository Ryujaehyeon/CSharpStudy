﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertiesInterface
{
    interface INameValue
    {
        string Name { get; set; }
        string Value { get; set; }

    }
    class NameValue:INameValue
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
