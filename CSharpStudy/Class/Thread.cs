﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace CSharpStudy.Class
{
    class Counter
    {
        const int LOOP_COUNT = 1000;
        readonly object thisLock;
        bool lockedCount = false;

        private int count;
        public int Count { get { return count; } }

        public Counter()
        {
            thisLock = new object();
            count = 0;
        }

        public void Increase()
        {
            int loopCount = LOOP_COUNT;
            while (loopCount-- > 0)
            {
                //lock 또는 monitor를 사용할 때
                //lock
                //lock (thisLock)
                //    count++;
                //
                //moniter
                //Monitor.Enter(thisLock);
                //try
                //{
                //    count++;
                //}
                //finally
                //{
                //    Monitor.Exit(thisLock);
                //}

                //monitor.wait(), pulse()
                lock (thisLock)
                {
                    while (count > 0 || lockedCount == true)
                        Monitor.Wait(thisLock);

                    lockedCount = true;     //잠금
                    count++;                //작업내용
                    lockedCount = false;    //해제

                    Monitor.Pulse(thisLock);
                }
            }
            Console.WriteLine(count);
            Thread.Sleep(1);
        }

        public void Decrease()
        {
            int loopCount = LOOP_COUNT;
            while (loopCount-- > 0)
            {
                //lock 또는 monitor를 사용할 때
                //lock
                //lock (thisLock)
                //    count--;
                //
                //monitor
                //Monitor.Enter(thisLock);
                //try
                //{
                //    count--;
                //}
                //finally
                //{
                //    Monitor.Exit(thisLock);
                //}

                //monitor.wait(), pulse()
                lock (thisLock)
                {
                    while (count < 0 || lockedCount == true)
                        Monitor.Wait(thisLock);

                    lockedCount = true;     //잠금
                    count--;                //작업내용
                    lockedCount = false;    //해제
                    Monitor.Pulse(thisLock);
                }
            }
            Console.WriteLine(count);
            Thread.Sleep(1);
        }
    }
}