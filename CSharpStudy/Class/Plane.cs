﻿using System;

namespace CSharpStudy
{
    class Plane : IFlyable, IRide
    {
        public void Fly()
        {
            Console.WriteLine("Fly!");
        }

        public void Ride()
        {
            Fly();
        }
    }
}
