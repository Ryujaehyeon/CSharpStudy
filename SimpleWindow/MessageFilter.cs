﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimpleWindow
{
    public class MessageFilter : IMessageFilter
    {
        public bool PreFilterMessage(ref Message m)
        {
            if (m.Msg == 0x0F || m.Msg == 0xA0 || 
                m.Msg == 0x200 || m.Msg == 0x113)
                return false;

            Console.WriteLine("{0} : {1}", m.ToString(), m.Msg);

            if (m.Msg == 0x201)
                Application.Exit();

            return true;

            //특정 메시지를 제외 조건
            //WM_PAINT(0x0F), WM_MOUSEMOVE(0x200), WM_TIMER(0x113)
            //아래 조건은 마우스 이동 부터, 좌우클릭, 가운데 버튼 동작 및 휠 제외
            //if(m.Msg >= 0x200 && m.Msg < 0x20E)
            //{
            //    Console.WriteLine("발생한 메시지 : " + m.Msg);
            //    return true;
            //}
            //return false;


        }
    }
}
