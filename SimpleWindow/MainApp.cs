﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SimpleWindow
{
    class MainApp : Form
    {
        //public void MyMouseHandler(object sender, MouseEventArgs e)
        //{
        //    Console.WriteLine("Sender : {0}", ((Form)sender).Text);
        //    Console.WriteLine("X:{0}, Y:{1}", e.X, e.Y);
        //    Console.WriteLine("Button:{0}, Clicks:{1}", e.Button, e.Clicks);
        //    Console.WriteLine();
        //}

        //public MainApp()
        //{

        //}
        //public MainApp(string title)
        //{
        //    this.Text = title;
        //    this.MouseDown +=
        //        new MouseEventHandler(MyMouseHandler);
        //}

        static void Main(string[] args)
        {
            MainApp form = new MainApp();

            form.Width = 300;
            form.Height = 200;

            form.MouseDown += new MouseEventHandler(WindowSizeCustomization.Form_MouseDown);

            Application.Run(form);
            //
            //form.Click += new EventHandler(
            //    (sender, eventArgs) =>
            //    {
            //        Console.WriteLine("Closing Window...");
            //        Application.Exit();
            //    });

            //Console.WriteLine("Start Window application...");
            //Application.AddMessageFilter(new MessageFilter());
            //Application.Run(form);
            //Console.WriteLine("End Window application...");
        }
    }
}
