﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace FormCustomization
{
    class MainApp : Form
    {
        Random rand;
        bool Sizeflg;//전체화면 flag, True = 전체 화면, False = 기본 사이즈
        public MainApp()
        {
            rand = new Random();

            this.MinimumSize = new Size(800, 600);
            this.MaximumSize = new Size(1920, 1080);

            Sizeflg = false;

            this.MouseWheel += new MouseEventHandler(MainApp_MouseWheel);
            this.MouseDown += new MouseEventHandler(MainApp_MouseDown);
            this.KeyDown += new KeyEventHandler(MainApp_KeyDown);
        }

        void MainApp_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Color oldColor = this.BackColor;
                this.BackColor = Color.FromArgb(rand.Next(0, 255),
                                                rand.Next(0, 255),
                                                rand.Next(0, 255));
            }
            else if (e.Button == MouseButtons.Right)
            {
                if (this.BackgroundImage != null)
                {
                    this.BackgroundImage = null;
                    return;
                }

                string file = "Sample.png";
                if (System.IO.File.Exists(file) == false)
                    MessageBox.Show("이미지 파일이 없습니다.");
                else
                    this.BackgroundImage = Image.FromFile(file);
            }
        }
        void MainApp_MouseWheel(object sender, MouseEventArgs e)
        {
            double tmpOpacity = this.Opacity + (e.Delta > 0 ? 0.01 : -0.01);
            this.Opacity = tmpOpacity;
            //opacity가 0.000 미만의 소수점일때 0.01로 끌어올림
            if (this.Opacity < 0.001)
                this.Opacity = 0.01f;
            Console.WriteLine("Opeacity : {0}", this.Opacity);
        }
        void MainApp_KeyDown(object sender, KeyEventArgs e)
        {
            Form form = (Form)sender;

            //Modifiers는 해당키에 추가로 서브키를 더받게 해줌
            switch (e.Modifiers)
            {
                case Keys.Alt when e.KeyCode == Keys.Enter:
                    if (Sizeflg)
                    {
                        //테두리 생성 및 삭제 : 생성
                        //윈도우 기본값 Normal, Minimized 로 바꾸면 최소화
                        form.FormBorderStyle = FormBorderStyle.Sizable;
                        form.WindowState = FormWindowState.Normal;
                        //form.Width = form.MinimumSize.Width;
                        //form.Height = form.MinimumSize.Height;

                        //창 숨기기
                        //this.Hide();
                        Sizeflg = !Sizeflg;
                    }
                    else
                    {
                        //테두리 생성 및 삭제 : 삭제
                        form.FormBorderStyle = FormBorderStyle.None;
                        //윈도우 전체화면
                        form.WindowState = FormWindowState.Maximized;
                        //form.Width = form.MaximumSize.Width;
                        //form.Height = form.MaximumSize.Height;
                        Sizeflg = !Sizeflg;

                        //창 보이기
                        //hide()시 활성화가 풀려서 의미가 없는듯 하다
                        //this.Show();
                    }

                    break;
                case Keys.Alt when e.KeyCode == Keys.F4:
                    //alt + F4 안먹히게
                    e.Handled = true;
                    break;
                default:
                    break;
            }
        }
        static void Main(string[] args)
        {
            Application.Run(new MainApp());
        }

    }
}
